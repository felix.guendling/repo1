project(repo1)
cmake_minimum_required(VERSION 3.11)

add_library(repo1 STATIC src/repo1.cc)
target_link_libraries(repo1 repo3)
target_include_directories(repo1 PUBLIC include)