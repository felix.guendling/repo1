#include "repo1/repo1.h"

#include "repo3/repo3.h"

namespace repo1 {

int repo1() { return 1 + repo3::repo3(); }

}  // namespace repo1